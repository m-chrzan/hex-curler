require 'sinatra'
require 'sinatra/cookies'

require './hex_engine'

def secret
  ENV['HEX_SECRET'] || 'tajny_token_hehe'
end

before do
  headers 'Content-Type' => 'text/plain'
  if request.user_agent !~ /^curl/
    halt <<~MSG
Welcome to Hex Curler!
======================
This game is based on Jeff Moore's Hex: www.1km1kt.net/rpg/hex
I wrote a technical post about it: m-chrzan.xyz/blog/hex-curler.html
Source code: gitlab.com/m-chrzan/hex-curler

This game is meant to be experienced in the command line. Run the following line
of bash to get started:

c=; while clear; curl -c k -b k hex.m-chrzan.xyz/$c; read c; [ $c ]; do :; done
    MSG
  end
end

get '/' do
  <<~MSG
Welcome to Hex Curler!
======================
This game is based on Jeff Moore's Hex: www.1km1kt.net/rpg/hex
I wrote a technical post about it: m-chrzan.xyz/blog/hex-curler.html
Source code: gitlab.com/m-chrzan/hex-curler

How to Play
===========
If you're not using it already, run this line of bash to start playing:

c=; while clear; curl -c k -b k hex.m-chrzan.xyz/$c; read c; [ $c ]; do :; done

You're about to start crawling through a dungeon, fighting monsters and finding
treasure. Your character has three attributes:
  - Health
  - Endurance
  - eXperience

After each move you'll be presented with a menu of options that could look
something like this:
  >[r]un away
  >[f]ight (-1 H, -1 E, 1 X)

Select a move by typing the highlighted letter and hitting enter (e.g. type r if
you want to [r]un away like a wuss).

To restart the game, run `rm k` to remove the cookie that stores your game state.

>[c]ontinue
  MSG
end

get '/:command' do |command|
  begin
    engine = Hex.new cookies.to_h
  rescue InvalidChecksumError
    halt 422, <<~MSG
      Invalid state, checksum does not match!
      If you lost your last valid game state, you'll have to remove your current
      cookie and start over.
    MSG
  end

  engine.step command
  engine.state_h.each_pair do |key, value|
    cookies[key] = value
  end
  engine.message
end
